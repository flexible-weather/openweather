# OpenWeather



## Getting started

**Weather App** is a fully functional Android app built entirely with Kotlin and Jetpack Compose. It
follows Android design and development best practices.

The key idea is to have one single activity that serves as the main entry to and exit from **weather** app. The Weather Home screen and Search screens are implemented with **Jetpack Compose** without using fragments.

**MVVM** and Clean Architecture (CA) are two architectural patterns commonly used in Android app development. Combining these two patterns can lead to a well-structured, maintainable and testable Android application. 𝗠𝗩𝗩𝗠 separates your app into three main components — Model, View, and ViewModel.

## Screenshots

### Dark Theme

![flashscreen](https://i.postimg.cc/Vv7h00sY/weather-flashscreen-dark.png)

![Home](https://i.postimg.cc/YCBPG7ML/home-dark.png)

![Search](https://i.postimg.cc/B65TVrRk/weather-search-dark.png)


## License
open source project

## Project status

