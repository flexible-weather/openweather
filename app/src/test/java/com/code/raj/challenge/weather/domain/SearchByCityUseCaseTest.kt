package com.code.raj.challenge.weather.domain

import com.code.raj.challenge.weather.cache.search.SearchPreference
import com.code.raj.challenge.weather.compose.model.CitySearchData
import com.code.raj.challenge.weather.viewmodels.WeatherHomeScreenViewModel
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerifyAll
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import io.mockk.slot
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test


class SearchByCityUseCaseTest {

    @MockK
    private lateinit var searchPreference: SearchPreference

    @MockK
    private lateinit var weatherHomeUseCase:WeatherHomeUseCase


    private lateinit var searchByCityUseCase: SearchByCityUseCase
    init {
        MockKAnnotations.init(this, relaxUnitFun = true)
    }

    @Before
    fun setUp() {
        searchByCityUseCase = SearchByCityUseCase(
            searchPreference,
            weatherHomeUseCase
        )
    }

    @Test
    fun invoke_triggersServiceCall() = runTest {
        val city = "plano"
        val latitude = "38.33"
        val longitude = "-22.89"
        val citySlot = slot<CitySearchData>()
        val viewState = mockk<WeatherHomeScreenViewModel.ViewState>()
        coEvery { weatherHomeUseCase.loadWeatherByPreferredLocation() } returns flowOf(viewState)

        searchByCityUseCase.invoke(city, latitude, longitude).collect()

        coVerifyAll {
            searchPreference.saveRecentSearchCity(capture(citySlot))
            weatherHomeUseCase.loadWeatherByPreferredLocation()
        }
        citySlot.captured.run {
            assertEquals(city, address)
            coord?.let {
                assertEquals(latitude.toDouble(), it.lat)
                assertEquals(longitude.toDouble(), it.lon)
            }
        }
    }
}