package com.code.raj.challenge.weather.viewmodels

import com.code.raj.challenge.weather.compose.model.CitySearchData
import com.code.raj.challenge.weather.domain.WeatherSearchUseCase
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class CitySearchViewModelTest {

    @MockK
    private lateinit var weatherSearchUseCase: WeatherSearchUseCase

    private val unconfinedTestDispatcher = UnconfinedTestDispatcher()

    private lateinit var viewModel: CitySearchViewModel

    init {
        MockKAnnotations.init(this, relaxUnitFun = true)
    }

    @Before
    fun setUp() {
        Dispatchers.setMain(unconfinedTestDispatcher)
        viewModel = CitySearchViewModel(weatherSearchUseCase)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun searchCities_withCity_resultsListOfCities() {
        val cityName = "plano"
        val citySearchData = mockk<CitySearchData>()
        coEvery { weatherSearchUseCase.invoke(cityName) } returns flowOf(
            mutableListOf(
                citySearchData
            )
        )

        viewModel.searchCities(cityName)

        coVerify {
            weatherSearchUseCase.invoke(cityName)
        }

        val result = viewModel.citySearchResult.value

        Assert.assertEquals(1, result.size)
        Assert.assertEquals(citySearchData, result[0])
    }

    @Test
    fun clearSearch_clearTheEnteredCity() {
        viewModel.clearSearch()

        val result = viewModel.citySearchResult.value
        Assert.assertTrue(result.isEmpty())

    }
}