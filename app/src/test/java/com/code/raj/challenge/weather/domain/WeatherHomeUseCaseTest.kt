package com.code.raj.challenge.weather.domain

import android.location.Location
import com.code.raj.challenge.weather.cache.model.Coord
import com.code.raj.challenge.weather.cache.search.SearchPreference
import com.code.raj.challenge.weather.compose.model.CitySearchData
import com.code.raj.challenge.weather.repository.WeatherRepository
import com.code.raj.challenge.weather.utils.LocalLocationManager
import io.mockk.Called
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class WeatherHomeUseCaseTest {

    @MockK
    private lateinit var searchPreference: SearchPreference

    @MockK
    private lateinit var locationManager: LocalLocationManager

    @MockK
    private lateinit var weatherRepository: WeatherRepository

    private val standardTestDispatcher = StandardTestDispatcher()

    private lateinit var weatherHomeUseCase: WeatherHomeUseCase
    init {
        MockKAnnotations.init(this, relaxUnitFun = true)
    }

    @Before
    fun setUp() {
        Dispatchers.setMain(standardTestDispatcher)
        weatherHomeUseCase = WeatherHomeUseCase(
            locationManager,
            weatherRepository,
            searchPreference,
            standardTestDispatcher
        )
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun invoke_onLocationEnabled_loadsCurrentLocationWeatherResult() = runTest {
        val location = mockk<Location>()
        val latitude = 33.22
        val longitude = -22.55
        every { location.latitude } returns latitude
        every { location.longitude } returns longitude
        every { locationManager.areLocationPermissionGranted() } returns true
        coEvery { locationManager.fetchUpdates()} returns flowOf(location)
        coEvery { weatherRepository.getWeatherByLatLong(any(), any()) } returns flowOf(mockk())
        coEvery { weatherRepository.forecastWeather(any(), any()) } returns flowOf(mockk())

        weatherHomeUseCase.invoke().collect()

        coVerify {
            locationManager.fetchUpdates()

            weatherRepository.getWeatherByLatLong(latitude.toString(), longitude.toString())
            weatherRepository.forecastWeather(latitude.toString(), longitude.toString())
        }

    }

    @Test
    fun invoke_onLocationNotEnabled_notFetchLocationUpdates() = runTest {
        every { locationManager.areLocationPermissionGranted() } returns false
        coEvery { weatherRepository.getWeatherByLatLong(any(), any()) } returns flowOf(mockk())
        coEvery { weatherRepository.forecastWeather(any(), any()) } returns flowOf(mockk())

        weatherHomeUseCase.invoke().collect()

        coVerify {
            locationManager.fetchUpdates() wasNot Called
        }
    }

    @Test
    fun invoke_locationNotEnabledWithLastSearchCity_loadsWeather() = runTest {
        every { locationManager.areLocationPermissionGranted() } returns false
        val citySearchData = mockk<CitySearchData>()
        val coord = mockk<Coord>()
        every { searchPreference.getRecentSearchCity() } returns citySearchData
        every { citySearchData.coord } returns coord
        every { coord.lat } returns 28.89
        every { coord.lon } returns -19.6

        coEvery { weatherRepository.getWeatherByLatLong(any(), any()) } returns flowOf(mockk())
        coEvery { weatherRepository.forecastWeather(any(), any()) } returns flowOf(mockk())

        weatherHomeUseCase.invoke().collect()

        coVerify {
            searchPreference.getRecentSearchCity()

            weatherRepository.getWeatherByLatLong(any(), any())
            weatherRepository.forecastWeather(any(), any())
        }
    }

    @Test
    fun invoke_noLastKnownLocationAndCity_notTriggerAnyWeatherService() = runTest {
        every { locationManager.areLocationPermissionGranted() } returns false
        every { searchPreference.getRecentSearchCity() } returns null

        weatherHomeUseCase.invoke().collect()

        coVerify {
            weatherRepository.getWeatherByLatLong(any(), any()) wasNot Called
            weatherRepository.forecastWeather(any(), any()) wasNot Called
        }
    }

    @Test
    fun loadWeatherByPreferredLocation_withRecentSearchedCity_loadsWeather() = runTest {
        val citySearchData = mockk<CitySearchData>()
        val coord = mockk<Coord>()
        every { searchPreference.getRecentSearchCity() } returns citySearchData
        every { citySearchData.coord } returns coord
        every { coord.lat } returns 28.89
        every { coord.lon } returns -19.6

        coEvery { weatherRepository.getWeatherByLatLong(any(), any()) } returns flowOf(mockk())
        coEvery { weatherRepository.forecastWeather(any(), any()) } returns flowOf(mockk())

        weatherHomeUseCase.loadWeatherByPreferredLocation()

        coVerify {
            weatherRepository.getWeatherByLatLong(any(), any())
            weatherRepository.forecastWeather(any(), any())
        }

    }
}