package com.code.raj.challenge.weather.viewmodels

import com.code.raj.challenge.weather.domain.SearchByCityUseCase
import com.code.raj.challenge.weather.domain.WeatherHomeUseCase
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class WeatherHomeScreenViewModelTest {

    @RelaxedMockK
    private lateinit var weatherHomeUseCase: WeatherHomeUseCase

    @MockK
    private lateinit var searchByCityUseCase: SearchByCityUseCase

    private lateinit var viewState:WeatherHomeScreenViewModel.ViewState
    private val unconfinedTestDispatcher = UnconfinedTestDispatcher()

    private lateinit var viewModel: WeatherHomeScreenViewModel

    init {
        MockKAnnotations.init(this, relaxUnitFun = true)
    }

    @Before
    fun setUp() {
        Dispatchers.setMain(unconfinedTestDispatcher)

        viewState = mockk<WeatherHomeScreenViewModel.ViewState>()
        coEvery { viewState.isLoading } returns false
        coEvery { viewState.weatherResult } returns mockk()
        coEvery { viewState.forecastResponse } returns mockk()

        viewModel = WeatherHomeScreenViewModel(
            weatherHomeUseCase,
            searchByCityUseCase
        )
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun getFlashScreenState() {
    }

    @Test
    fun getViewState() {
    }

    @Test
    fun refresh() {
    }

    @Test
    fun loadData_onResult_updatesViewStateWithResult() {
        coEvery { weatherHomeUseCase.invoke() } returns flowOf(viewState)
        assertEquals(true, viewModel.viewState.value.isLoading)

        viewModel.loadData()

        coVerify {
            weatherHomeUseCase.invoke()
        }
        assertEquals(false, viewModel.viewState.value.isLoading)
        assertNotNull(viewModel.viewState.value.weatherResult)
        assertNotNull(viewModel.viewState.value.forecastResponse)
    }

    @Test
    fun searchByCity_withCityData_updatesViewStateWithCityWeatherResult() {
        coEvery { searchByCityUseCase.invoke(any(), any(), any()) } returns flowOf(viewState)

        viewModel.searchWeatherByCity("plano", "42.88", "-23.80")

        coVerify {
            searchByCityUseCase.invoke(any(), any(), any())
        }

        assertEquals(false, viewModel.viewState.value.isLoading)
        assertNotNull(viewModel.viewState.value.weatherResult)
        assertNotNull(viewModel.viewState.value.forecastResponse)
    }
}