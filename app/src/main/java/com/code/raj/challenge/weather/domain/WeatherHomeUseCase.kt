package com.code.raj.challenge.weather.domain

import com.code.raj.challenge.weather.cache.search.SearchPreference
import com.code.raj.challenge.weather.repository.WeatherRepository
import com.code.raj.challenge.weather.utils.LocalLocationManager
import com.code.raj.challenge.weather.viewmodels.WeatherHomeScreenViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

/**
 * This use case handles all the business logic to show on the home screen.
 */
class WeatherHomeUseCase @Inject constructor(
    private val localLocationManager: LocalLocationManager,
    private val weatherRepository: WeatherRepository,
    private val searchPreference: SearchPreference,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) {
    private fun locationEnabled() = localLocationManager.areLocationPermissionGranted()
    private fun recentSearchLocation() = searchPreference.getRecentSearchCity()

    suspend operator fun invoke(): Flow<WeatherHomeScreenViewModel.ViewState> {
        flowOf(WeatherHomeScreenViewModel.ViewState(isLoading = true))
        return if (locationEnabled()) {
            loadWeatherByLocation()
        } else if (recentSearchLocation() != null) {
            loadWeatherByPreferredLocation()
        } else {
            flowOf(WeatherHomeScreenViewModel.ViewState(isLoading = true))
        }.flowOn(dispatcher)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    private suspend fun loadWeatherByLocation(): Flow<WeatherHomeScreenViewModel.ViewState> {
        return localLocationManager.fetchUpdates()
            .flatMapLatest { location ->
                val latitude = location.latitude.toString()
                val longitude = location.longitude.toString()
                combineWeather(latitude, longitude)
            }
    }

    suspend fun loadWeatherByPreferredLocation(): Flow<WeatherHomeScreenViewModel.ViewState> {
        val citySearchData = recentSearchLocation()
        return citySearchData?.coord?.let {
            combineWeather(it.lat.toString(), it.lon.toString())
        } ?: run {
            flowOf(WeatherHomeScreenViewModel.ViewState(isLoading = false))
        }
    }

    private suspend fun combineWeather(
        latitude: String,
        longitude: String
    ) = weatherByLatLong(latitude, longitude).combine(
        weatherForecast(latitude, longitude)
    ) { weatherResult, weatherResponse ->
        WeatherHomeScreenViewModel.ViewState(
            isLoading = weatherResult?.let { false } ?: true,
            weatherResult = weatherResult,
            forecastResponse = weatherResponse
        )
    }.catch { e ->
        e.printStackTrace()
        WeatherHomeScreenViewModel.ViewState(
            isLoading = false
        )
    }

    private suspend fun weatherForecast(
        latitude: String,
        longitude: String
    ) = weatherRepository.forecastWeather(latitude, longitude)

    private suspend fun weatherByLatLong(
        latitude: String,
        longitude: String
    ) = weatherRepository.getWeatherByLatLong(latitude, longitude)
}