package com.code.raj.challenge.weather

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.activity.viewModels
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.core.view.WindowCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.compose.rememberNavController
import com.code.raj.challenge.weather.compose.theme.WeatherAppTheme
import com.code.raj.challenge.weather.compose.ui.LocationPermissionsContent
import com.code.raj.challenge.weather.navigation.WeatherNavGraph
import com.code.raj.challenge.weather.navigation.navigateToHomeScreen
import com.code.raj.challenge.weather.viewmodels.MainActivityViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch


@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private val mainActivityViewModel: MainActivityViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        val splashScreen = installSplashScreen()
        WindowCompat.setDecorFitsSystemWindows(window, false)
        super.onCreate(savedInstanceState)

        var uiState: MainActivityViewModel.FlashScreenState by mutableStateOf(MainActivityViewModel.FlashScreenState.Loading)

        // Update the uiState
        lifecycleScope.launch {
            lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                mainActivityViewModel.flashScreenState
                    .onEach { uiState = it }
                    .collect()
            }
        }

        splashScreen.setKeepOnScreenCondition {
            when (uiState) {
                MainActivityViewModel.FlashScreenState.Loading -> true
                MainActivityViewModel.FlashScreenState.Success -> false
            }
        }

        enableEdgeToEdge()

        setContent {
            WeatherAppTheme {
                val navController = rememberNavController()
                LocationPermissionsContent(this) {
                    navController.navigateToHomeScreen()
                }
                WeatherNavGraph(navController)
            }
        }
    }
}