package com.code.raj.challenge.weather.di

import com.code.raj.challenge.weather.cache.search.SearchPreference
import com.code.raj.challenge.weather.cache.search.SearchPreferenceImpl
import com.code.raj.challenge.weather.repository.LocalNetworkWeatherRepository
import com.code.raj.challenge.weather.repository.NetworkWeatherSearchRepository
import com.code.raj.challenge.weather.repository.WeatherRepository
import com.code.raj.challenge.weather.repository.WeatherSearchRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class WeatherDataModule {

    @Binds
    abstract fun bindWeatherRepository(impl: LocalNetworkWeatherRepository): WeatherRepository

    @Binds
    abstract fun bindWeatherSearchRepository(impl: NetworkWeatherSearchRepository): WeatherSearchRepository

    @Binds
    abstract fun bindSearchRepository(impl: SearchPreferenceImpl) : SearchPreference

}