package com.code.raj.challenge.weather.compose.ui

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Card
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Divider
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.material.pullrefresh.PullRefreshIndicator
import androidx.compose.material.pullrefresh.pullRefresh
import androidx.compose.material.pullrefresh.rememberPullRefreshState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import coil.compose.AsyncImage
import coil.request.CachePolicy
import coil.request.ImageRequest
import com.code.raj.challenge.weather.R
import com.code.raj.challenge.weather.cache.model.Clouds
import com.code.raj.challenge.weather.cache.model.Coord
import com.code.raj.challenge.weather.cache.model.Main
import com.code.raj.challenge.weather.cache.model.Sys
import com.code.raj.challenge.weather.cache.model.Weather
import com.code.raj.challenge.weather.cache.model.Wind
import com.code.raj.challenge.weather.cache.model.forecast.WeatherForecastResult
import com.code.raj.challenge.weather.utils.Constants
import com.code.raj.challenge.weather.utils.DateTimeConverter
import com.code.raj.challenge.weather.utils.DateTimeConverter.convertLongToDay
import com.code.raj.challenge.weather.utils.DateTimeConverter.roundTwo
import com.code.raj.challenge.weather.utils.StringUtils
import com.code.raj.challenge.weather.utils.WeatherImageMapping
import com.code.raj.challenge.weather.viewmodels.WeatherHomeScreenViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

@OptIn(ExperimentalMaterialApi::class)
@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun WeatherHomeScreen(
    weatherViewModel: WeatherHomeScreenViewModel,
    navigateToSearch: () -> Unit
) {
    val weatherViewState = weatherViewModel.viewState.collectAsStateWithLifecycle()
    val refreshScope = rememberCoroutineScope()
    var refreshing by remember { mutableStateOf(false) }

    fun refresh() = refreshScope.launch {
        refreshing = true
        weatherViewModel.refresh()
        refreshing = weatherViewState.value.isLoading
    }

    val state = rememberPullRefreshState(refreshing, ::refresh)

    val weatherForecastResult = weatherViewState.value.forecastResponse
    val weatherResult = weatherViewState.value.weatherResult

    Scaffold(
        modifier = Modifier
            .fillMaxSize()
            .statusBarsPadding(),
        topBar = {
            val title = weatherResult?.let {
                "${it.name}, ${it.sys.country}"
            } ?: stringResource(R.string.home_title)
            WeatherHomeAppBar(title,
                onSearch = {
                    navigateToSearch()
                }
            )
        }) {
        Box(
            Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp)
                .pullRefresh(state)
        ) {
            //vertically scrollable content
            LazyColumn(Modifier.fillMaxSize()) {
                weatherResult?.let {
                    var weather: Weather? = null
                    if (weatherResult.weather.isNotEmpty()) {
                        weather = weatherResult.weather[0]
                    }
                    item {
                        WeatherHeaderSection(
                            code = weatherResult.cod,
                            coordinate = weatherResult.coord,
                            main = weatherResult.main,
                            todayWeather = weather,
                            weatherResult.dt
                        )
                        with(weatherResult) {
                            WeatherGrid(
                                main = main,
                                wind = wind,
                                clouds = clouds,
                                sys = sys,
                                visibility = visibility,
                                timeZoneOffset = weatherResult.timezone
                            )
                        }
                        weatherForecastResult?.let {
                            ThreeHourWeather(it)
                        }
                    }

                    weatherForecastResult?.let {
                        dayForecast(it)
                    }
                } ?: run {
                    if (weatherViewState.value.isLoading) {
                        item {
                            ProgressLoader()
                        }
                    } else {
                        item {
                            NoWeatherMessageBanner()
                        }
                    }
                }
            }
            //standard Pull-Refresh indicator. You can also use a custom indicator
            PullRefreshIndicator(refreshing, state, Modifier.align(Alignment.TopCenter))
        }
    }
}

@Composable
private fun NoWeatherMessageBanner() {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.padding(horizontal = 48.dp),
    ) {
        val message =
            "Unfortunately not find weather for the location. Please refresh..."
        Text(
            text = message,
            style = MaterialTheme.typography.subtitle1,
            textAlign = TextAlign.Center,
            modifier = Modifier.padding(vertical = 24.dp),
        )
    }
}

@Composable
private fun ProgressLoader() {
    Surface(
        modifier = Modifier
            .statusBarsPadding()
            .padding(top = 50.dp)
    ) {
        Box(
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.TopCenter
        ) {
            CircularProgressIndicator(color = MaterialTheme.colors.onPrimary)
        }
    }
}

@Composable
private fun WeatherConditionImage(code: Int) {
    val imageRequest = imageRequest(code)
    Box(
        modifier = Modifier
            .wrapContentSize(), contentAlignment = Alignment.Center
    ) {
        CachedImage(imageRequest)
    }
}

@Composable
private fun imageRequest(code: Int): ImageRequest {
    val imageUrl =
        Constants.IMAGE_URL.replace("code", WeatherImageMapping.getWeatherIconCode(code))
    return ImageRequest.Builder(LocalContext.current)
        .data(imageUrl)
        .dispatcher(Dispatchers.IO)
        .memoryCachePolicy(CachePolicy.ENABLED)
        .build()
}

@Composable
private fun CachedImage(imageRequest: ImageRequest, modifier: Modifier = Modifier) {
    AsyncImage(
        model = imageRequest,
        contentDescription = null,
        contentScale = ContentScale.FillWidth,
        modifier = modifier
    )
}

@Composable
private fun WeatherHeaderSection(
    code: Int,
    coordinate: Coord,
    main: Main,
    todayWeather: Weather?,
    dt: Long
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 20.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Row(modifier = Modifier.wrapContentWidth()) {
            WeatherConditionImage(code)
            todayWeather?.let { weather ->
                val descString = StringUtils.capitalizeFirstChar(weather.description)
                Text(
                    text = descString,
                    textAlign = TextAlign.Center,
                    style = MaterialTheme.typography.subtitle1,
                    modifier = Modifier.padding(5.dp)
                )
            }
        }
        Spacer(modifier = Modifier.size(4.dp))
        Row(modifier = Modifier.wrapContentSize()) {
            Text(
                text = "${main.temp.toInt()}°F",
                style = MaterialTheme.typography.h1,
                modifier = Modifier.offset(y = (-24).dp)
            )
        }
        Text(
            text = DateTimeConverter.convertLongToDate(dt),
            modifier = Modifier.wrapContentWidth(),
            style = MaterialTheme.typography.subtitle2,
            color = Color.Red
        )
        todayWeather?.let {
            Text(
                text = it.main,
                style = MaterialTheme.typography.subtitle2,
                modifier = Modifier.padding(vertical = 15.dp)
            )
        }
        Text(
            text = "H:${coordinate.lon?.roundTwo()}°  L:${coordinate.lat?.roundTwo()}°",
            style = MaterialTheme.typography.h6,
        )
    }
}

@Composable
private fun WeatherGrid(
    main: Main,
    wind: Wind,
    clouds: Clouds,
    sys: Sys,
    timeZoneOffset: Int,
    visibility: Int
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 1.dp, vertical = 8.dp),
        elevation = 8.dp,
        shape = RectangleShape
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
            ) {
                WeatherGridCard(item = "SunRise:${DateTimeConverter.toTime(sys.sunrise, timeZoneOffset)}AM")
                WeatherGridCard(item = "Set:${DateTimeConverter.toTime(sys.sunset, timeZoneOffset)}PM")
                WeatherGridCard("Tmp:${main.temp}\u2109")
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
            ) {
                WeatherGridCard(item = "Wind: ${wind.speed}m")
                WeatherGridCard(item = "Humidity: ${main.humidity}%")
                WeatherGridCard(item = "Visibility: $visibility")
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
            ) {
                WeatherGridCard(item = "Feels: ${main.feelsLike}\u2109")
                WeatherGridCard(item = "Pressure: ${main.pressure}")
                WeatherGridCard(item = "Cloudiness: ${clouds.all}%")
            }
        }
    }
}

@Composable
private fun WeatherGridCard(item: String) {
    Text(
        text = item,
        style = MaterialTheme.typography.h3.copy(fontSize = 16.sp),
        textAlign = TextAlign.Center,
        modifier = Modifier.padding(8.dp)
    )
}

@Composable
fun WeatherHomeAppBar(title: String, onSearch: () -> Unit) {
    TopAppBar(
        modifier = Modifier.statusBarsPadding(),
        title = { Text(text = title, style = MaterialTheme.typography.h6) },
        backgroundColor = Color.Transparent,
        elevation = 0.dp,
        actions = {
            Box(
                modifier = Modifier
                    .wrapContentSize(),
                contentAlignment = Alignment.TopEnd
            ) {
                Row(modifier = Modifier.wrapContentSize()) {
                    IconButton(onClick = { onSearch() }) {
                        Icon(
                            Icons.Filled.Search,
                            contentDescription = null
                        )
                    }
                }
            }
        }
    )
}

fun LazyListScope.dayForecast(forecastResponse: WeatherForecastResult) {
    val dailyList = forecastResponse.forecastList
    fun groupDayData(): Map<String?, Triple<Double?, Double?, Int>> {
        return dailyList
            .groupBy { it.date }
            .mapValues { (_, main) ->
                main.map { it.weather[0].id to it.main }
            }.mapValues { (_, mainList) ->
                val max = mainList.maxBy { it.second!!.tempMax }
                val min = mainList.minBy { it.second!!.tempMin }
                Triple(max.second?.tempMax, min.second?.tempMin, max.first)
            }
    }

    val dailyData = groupDayData()
    val days = dailyData.keys.toList()
    items(days) { dayForecast ->
        Box(
            Modifier
                .fillMaxWidth()
                .padding(horizontal = 32.dp, vertical = 8.dp)
        ) {
            Text(
                text = convertLongToDay(dayForecast!!),//SimpleDateFormat("E, MMM dd").format(dayForecast),
                style = MaterialTheme.typography.subtitle1,
                modifier = Modifier.align(Alignment.CenterStart)
            )

            val temp = dailyData[dayForecast]

            Text(
                text = "${temp?.first?.toInt()}\u2109 / ${temp?.second?.toInt()}\u2109",
                style = MaterialTheme.typography.subtitle1,
                modifier = Modifier.align(Alignment.Center)
            )

            val imageModifier = Modifier
                .size(32.dp)
                .align(Alignment.CenterEnd)
            CachedImage(
                imageRequest = imageRequest(temp!!.third),
                modifier = imageModifier
            )
        }
        Divider()
    }
}

@Composable
fun ThreeHourWeather(forecastResponse: WeatherForecastResult) {
    val hourlyList = forecastResponse.forecastList
    LazyRow(modifier = Modifier.padding(vertical = 16.dp)) {
        items(hourlyList) {
            Box(modifier = Modifier.wrapContentWidth()) {
                Column(modifier = Modifier.padding(start = 4.dp, end = 4.dp)) {
                    Text(
                        text = SimpleDateFormat("hhaa", Locale.US).format(Date(it.dt!! * 1_000)),
                        style = MaterialTheme.typography.subtitle1
                    )

                    CachedImage(
                        imageRequest(code = it.weather[0].id),
                        modifier = Modifier
                            .size(32.dp)
                    )
                    Text(
                        text = "${it.main?.temp?.toInt()} \u2109",
                        style = MaterialTheme.typography.subtitle1
                    )
                }
            }
        }
    }
}