package com.code.raj.challenge.weather.cache.model

data class Weather(
    var id: Int,
    var main: String,
    var description: String,
    var icon: String

)