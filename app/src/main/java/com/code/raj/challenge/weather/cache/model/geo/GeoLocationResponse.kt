package com.code.raj.challenge.weather.cache.model.geo


data class GeoLocationResponse(
  var name: String? = null,
  var localNames: LocalNames? = LocalNames(),
  var lat: Double? = null,
  var lon: Double? = null,
  var country: String? = null,
  var state: String? = null

)