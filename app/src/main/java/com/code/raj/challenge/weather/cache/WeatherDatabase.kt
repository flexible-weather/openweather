package com.code.raj.challenge.weather.cache

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.code.raj.challenge.weather.cache.model.WeatherResult
import com.code.raj.challenge.weather.cache.model.forecast.WeatherForecastResult
import com.code.raj.challenge.weather.utils.RoomTypeConverters

/**
 * Local cache to maintain weather data by location latitude or longitude.
 * This cache is used to maintain current and forecast weather cache.
 */
@Database(entities = [WeatherResult::class, WeatherForecastResult::class], version = 1, exportSchema = false)
@TypeConverters(RoomTypeConverters::class)
abstract class WeatherDatabase: RoomDatabase() {

    abstract fun weatherDao():  WeatherDao
}