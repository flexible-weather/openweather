package com.code.raj.challenge.weather.utils

import androidx.room.TypeConverter
import com.code.raj.challenge.weather.cache.model.Weather
import com.code.raj.challenge.weather.cache.model.forecast.ForecastList
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


class RoomTypeConverters {

    @TypeConverter
    fun storedStringToMyObjects(data: String?): ArrayList<Weather>? {
        val gson = Gson()
        if (data == null) {
            return ArrayList<Weather>()
        }
        val listType = object : TypeToken<List<Weather>?>() {}.type
        return gson.fromJson<ArrayList<Weather>>(data, listType)
    }

    @TypeConverter
    fun myObjectsToStoredString(myObjects: List<Weather>?): String? {
        val gson = Gson()
        return gson.toJson(myObjects)
    }

    @TypeConverter
    fun toForecastList(data: String?): ArrayList<ForecastList>? {
        val gson = Gson()
        if (data == null) {
            return ArrayList<ForecastList>()
        }
        val listType = object : TypeToken<List<ForecastList>?>() {}.type
        return gson.fromJson<ArrayList<ForecastList>>(data, listType)
    }

    @TypeConverter
    fun fromForecastList(myObjects: List<ForecastList>?): String? {
        val gson = Gson()
        return gson.toJson(myObjects)
    }
}
