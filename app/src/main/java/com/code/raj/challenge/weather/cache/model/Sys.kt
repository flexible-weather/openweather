package com.code.raj.challenge.weather.cache.model

data class Sys(
    var country: String = "",
    var sunrise: Long = 0,
    var sunset: Long = 0
)