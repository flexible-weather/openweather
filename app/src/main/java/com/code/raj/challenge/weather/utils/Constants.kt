package com.code.raj.challenge.weather.utils

object Constants {
    const val BASE_URL = "https://api.openweathermap.org"
    const val IMAGE_URL = "https://openweathermap.org/img/wn/code@2x.png"
    const val API_KEY = "512562b10505342395ae75bca2569f0b"
    const val APP_ID = "appid"
}