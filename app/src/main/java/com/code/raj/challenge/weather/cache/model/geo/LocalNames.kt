package com.code.raj.challenge.weather.cache.model.geo

data class LocalNames(
    var fa: String? = null,
    var en: String? = null
)