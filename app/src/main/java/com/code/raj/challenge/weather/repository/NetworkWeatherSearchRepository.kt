package com.code.raj.challenge.weather.repository

import com.code.raj.challenge.weather.cache.model.WeatherResult
import com.code.raj.challenge.weather.compose.model.CitySearchData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

/**
 * Handles network calls to find the searched cities and
 * reverse the geo location to find the city's state and country.
 */
class NetworkWeatherSearchRepository @Inject constructor(
    private val weatherNetworkService: WeatherNetworkService
) : WeatherSearchRepository {

    override suspend fun getCitySearchResult(cityName: String) = flow {
        val list = mutableListOf<CitySearchData>()
        weatherNetworkService.searchCities(cityName).list.forEach { weatherResult: WeatherResult ->
            var citySearchData = CitySearchData(weatherResult.id, coord = weatherResult.coord)
            with(weatherResult.coord) {
                val geoLocationResponse =
                    weatherNetworkService.reverseGeoLocation(this.lat.toString(), this.lon.toString())
                geoLocationResponse.first().apply {
                    citySearchData = citySearchData.copy(address = "$name, $state, $country")
                }
                list.add(citySearchData)
            }
        }
        emit(list)
    }.flowOn(Dispatchers.IO)
}