package com.code.raj.challenge.weather.cache

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.code.raj.challenge.weather.cache.model.WeatherResult
import com.code.raj.challenge.weather.cache.model.forecast.WeatherForecastResult
import kotlinx.coroutines.flow.Flow

/**
 * WeatherDao interface is used to connect with location storage,
 * exclusively from local storage to support offline access.
 */
@Dao
interface WeatherDao {

    @Query("select * from weather_result where weather_lat =:lat AND weather_lon=:lon")
    fun getWeather(lat: Double, lon: Double): Flow<WeatherResult?>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertWeather(weatherList: WeatherResult)

    @Query("DELETE from weather_result where weather_lat =:lat AND weather_lon=:lon")
    fun deleteWeather(lat: Double, lon: Double)

    @Query("select * from weather_forecast where lat =:lat AND lon=:lon")
    fun getWeatherForecast(lat: String, lon: String): Flow<WeatherForecastResult?>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertWeatherForecast(weatherForecastResult: WeatherForecastResult)

    @Query("DELETE from weather_forecast where lat =:lat AND lon=:lon")
    fun deleteWeatherForecast(lat: String, lon: String)

//    @Query("DELETE FROM WeathResults")
//    suspend fun deleteAllWeather()
}