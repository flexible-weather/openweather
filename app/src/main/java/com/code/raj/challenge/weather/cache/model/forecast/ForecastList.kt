package com.code.raj.challenge.weather.cache.model.forecast

import com.code.raj.challenge.weather.cache.model.Clouds
import com.code.raj.challenge.weather.cache.model.Main
import com.code.raj.challenge.weather.cache.model.Weather
import com.code.raj.challenge.weather.cache.model.Wind
import com.google.gson.annotations.SerializedName

data class ForecastList(

    var dt: Long? = 0,
    var main: Main? = Main(),
    var weather: ArrayList<Weather> = arrayListOf(),
    var clouds: Clouds? = Clouds(),
    var wind: Wind? = Wind(),
    var visibility: Int? = 0,
    var pop: Double? = 0.0,
    var rain: Rain? = Rain(),
    var sys: Sys? = Sys(),
    @SerializedName("dt_txt") var dtTxt: String? = null
) {
    val date: String?
        get() { return dtTxt?.split(" ")?.get(0) }
}