package com.code.raj.challenge.weather.navigation

import android.annotation.SuppressLint
import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.NavOptions
import androidx.navigation.compose.composable

const val WEATHER_HOME_ROUTE = "home"
const val CITY_SEARCH_ROUTE = "search"
const val NAV_CITY_KEY = "city"
const val NAV_LAT_KEY = "lat"
const val NAV_LONG_KEY = "long"

fun NavController.navigateToHomeScreen(navOptions: NavOptions? = null) = navigate(WEATHER_HOME_ROUTE, navOptions)
fun NavController.navigateToCitySearchScreen(navOptions: NavOptions? = null) = navigate(CITY_SEARCH_ROUTE, navOptions)

@Composable
fun WeatherNavGraph(
    navController: NavHostController) {
    WeatherNavHost(navController = navController)
}

fun NavGraphBuilder.homeScreen(
    navController: NavHostController
) {
    composable(route = WEATHER_HOME_ROUTE) {
        HomeRoute(navController)
    }
}

@SuppressLint("SuspiciousIndentation")
fun NavGraphBuilder.citySearchScreen(
    navController: NavHostController
) {
    composable(
        route = CITY_SEARCH_ROUTE
    ) {
        SearchRoute(navController)
    }
}
