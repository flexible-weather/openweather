package com.code.raj.challenge.weather.utils

import java.util.Locale

object StringUtils {
    fun capitalizeFirstChar(description: String) =
        description.split(" ").joinToString(" ") { desc ->
            desc.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.ROOT) else it.toString() }
        }
}