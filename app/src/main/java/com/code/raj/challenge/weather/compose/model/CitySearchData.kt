package com.code.raj.challenge.weather.compose.model

import com.code.raj.challenge.weather.cache.model.Coord

data class CitySearchData(
    val id: Int = 0,
    val address: String = "",
    val coord: Coord? = null
)
