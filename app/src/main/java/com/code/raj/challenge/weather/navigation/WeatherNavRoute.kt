package com.code.raj.challenge.weather.navigation

import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import com.code.raj.challenge.weather.compose.ui.SearchCityScreen
import com.code.raj.challenge.weather.compose.ui.WeatherHomeScreen
import com.code.raj.challenge.weather.viewmodels.CitySearchViewModel
import com.code.raj.challenge.weather.viewmodels.WeatherHomeScreenViewModel

@Composable
fun HomeRoute(
    navController: NavHostController,
    weatherHomeScreenViewModel: WeatherHomeScreenViewModel = hiltViewModel()
) {
    val backStackEntryState = navController.currentBackStackEntryAsState()
    val savedStateHandle = backStackEntryState.value?.savedStateHandle
    val resultedCity = savedStateHandle?.remove<String>(NAV_CITY_KEY)
    val resultedLat = savedStateHandle?.remove<String>(NAV_LAT_KEY)
    val resultedLong = savedStateHandle?.remove<String>(NAV_LONG_KEY)
    resultedCity?.let {
        weatherHomeScreenViewModel.searchWeatherByCity(it, resultedLat!!, resultedLong!!)
    }
    WeatherHomeScreen(
        weatherViewModel = weatherHomeScreenViewModel,
        navigateToSearch = {
            navController.navigateToCitySearchScreen()
        }
    )
}

@Composable
fun SearchRoute(
    navController: NavHostController,
    citySearchViewModel: CitySearchViewModel = hiltViewModel()
) {
    SearchCityScreen(
        viewModel = citySearchViewModel,
        popBackWithResult = {
            val savedStateHandle = navController.previousBackStackEntry?.savedStateHandle
            with(savedStateHandle) {
                this?.set(NAV_CITY_KEY, it.address)
                this?.set(NAV_LAT_KEY, it.coord?.lat.toString())
                this?.set(NAV_LONG_KEY, it.coord?.lon.toString())
            }
            navController.popBackStack()
        }
    ) {
        navController.popBackStack()
    }
}