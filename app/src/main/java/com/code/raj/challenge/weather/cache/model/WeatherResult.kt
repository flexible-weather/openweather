package com.code.raj.challenge.weather.cache.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
@Suppress("unused")
const val WeatherResultResponse = """
    {"coord":{"lon":149.1292,"lat":-35.2813},
    "weather":[
    {"id":802,"main":"Clouds",
    "description":"scattered clouds",
    "icon":"03n"
    }],
    "base":"stations",
    "main":{"temp":281.07,
    "feels_like":281.07,
    "temp_min":280.1,
    "temp_max":284.01,
    "pressure":1019,
    "humidity":94},
    "visibility":900,
    "wind":{"speed":0,"deg":0},
    "clouds":{"all":38},
    "dt":1690653607,
    "sys":{
    "type":2,"id":2004200,
    "country":"AU",
    "sunrise":1690664448,"sunset":1690701533
    },
    "timezone":36000,
    "id":2172517,
    "name":"City",
    "cod":200
    }
    """
@Entity(tableName = "weather_result")
data class WeatherResult(

    @Embedded(prefix = "weather_")
    var coord: Coord = Coord(),
    var weather: ArrayList<Weather> = arrayListOf<Weather>(),
    var base: String? = null,
    @Embedded(prefix = "weather_")
    var main: Main = Main(),
    var visibility: Int = 0,
    @Embedded(prefix = "weather_")
    var wind: Wind = Wind(),
    @Embedded(prefix = "weather_")
    var clouds: Clouds = Clouds(),
    var dt: Long = 0L,
    @Embedded(prefix = "weather_")
    var sys: Sys = Sys(),
    var timezone: Int = 0,
    @PrimaryKey
    var id: Int = 0,
    var name: String = "",
    var cod: Int = 0,
    var ttl: Long = System.currentTimeMillis()

)