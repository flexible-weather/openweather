package com.code.raj.challenge.weather.cache.model.forecast

data class Sys(
    var pod: String? = null
)