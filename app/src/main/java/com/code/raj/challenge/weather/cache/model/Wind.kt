package com.code.raj.challenge.weather.cache.model


data class Wind (

  var speed : Double? = 0.0,
  var deg   : Int?    = 0,
  var gust  : Double? = 0.0

)