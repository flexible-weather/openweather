package com.code.raj.challenge.weather.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.code.raj.challenge.weather.repository.WeatherRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import javax.inject.Inject

@HiltViewModel
class MainActivityViewModel @Inject constructor(
    weatherRepository: WeatherRepository
) : ViewModel() {

    val flashScreenState: StateFlow<FlashScreenState> = weatherRepository.weatherData.map {
        FlashScreenState.Success
    }.stateIn(
        scope = viewModelScope,
        initialValue = FlashScreenState.Loading,
        started = SharingStarted.WhileSubscribed(5_000),
    )

    sealed interface FlashScreenState {
        data object Loading : FlashScreenState
        data object Success : FlashScreenState
    }
}