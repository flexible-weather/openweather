package com.code.raj.challenge.weather.compose.ui

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.provider.Settings
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.material.AlertDialog
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.core.app.ActivityCompat.shouldShowRequestPermissionRationale
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.startActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver

@Composable
private fun ShowLocationPermissionRationale(activity: ComponentActivity, onClickAction: (Boolean) -> Unit) {
    AlertDialog(
        onDismissRequest = {
            //Logic when dismiss happens
            onClickAction(false)
        },
        title = {
            Text("Permission Required")
        },
        text = {
            Text("You need to approve this permission in order to show current location weather")
        },
        confirmButton = {
            TextButton(
                colors = ButtonDefaults.buttonColors(MaterialTheme.colors.onPrimary),
                onClick = {
                    onClickAction(false)
                //Logic when user confirms to accept permissions
                    openApplicationSettings(activity)
            }) {
                Text("OK")
            }
        },
        dismissButton = {
            TextButton(
                colors = ButtonDefaults.buttonColors(MaterialTheme.colors.onPrimary),
                onClick = {
                    onClickAction(false)
                //Logic when user denies to accept permissions
            }) {
                Text("LATER")
            }
        })
}

/**
 * Checks weather the location access is provided or not. If not shows the location request
 * dialog is displayed on the screen and handles the request permission.
 */
@Composable
fun LocationPermissionsContent(activity: ComponentActivity, onPermissionResult: (Boolean) -> Unit) {
    var locationPermissionsGranted by remember { mutableStateOf(areLocationPermissionsAlreadyGranted(activity)) }
    var shouldShowPermissionRationale by remember {
        mutableStateOf(
            shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION)
        )
    }

    var shouldDirectUserToApplicationSettings by remember {
        mutableStateOf(false)
    }

    var currentPermissionsStatus by remember {
        mutableStateOf(
            decideCurrentPermissionStatus(
                locationPermissionsGranted,
                shouldShowPermissionRationale
            )
        )
    }

    val locationPermissions = arrayOf(
        Manifest.permission.ACCESS_FINE_LOCATION
    )

    val locationPermissionLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.RequestMultiplePermissions(),
        onResult = { permissions ->
            Log.d("LocationPermissions", "onResult ---------- $permissions")
            locationPermissionsGranted = permissions.values.reduce { acc, isPermissionGranted ->
                acc && isPermissionGranted
            }

            if (!locationPermissionsGranted) {
                shouldShowPermissionRationale =
                    shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION)
                        .also {
                            if (!it) {
                                onPermissionResult(false)
                            }
                        }
            } else {
                onPermissionResult(true)
            }

            shouldDirectUserToApplicationSettings =
                !shouldShowPermissionRationale && !locationPermissionsGranted
            currentPermissionsStatus = decideCurrentPermissionStatus(
                locationPermissionsGranted,
                shouldShowPermissionRationale
            )
        })

    val lifecycleOwner = LocalLifecycleOwner.current
    DisposableEffect(key1 = lifecycleOwner, effect = {
        val observer = LifecycleEventObserver { _, event ->
            if (event == Lifecycle.Event.ON_START &&
                !locationPermissionsGranted &&
                !shouldShowPermissionRationale
            ) {
                locationPermissionLauncher.launch(locationPermissions)
            }
        }
        lifecycleOwner.lifecycle.addObserver(observer)
        onDispose {
            lifecycleOwner.lifecycle.removeObserver(observer)
        }
    }
    )
    if (shouldShowPermissionRationale) {
        ShowLocationPermissionRationale(activity) {
            shouldShowPermissionRationale = it
            onPermissionResult(locationPermissionsGranted)
        }
    }
    if (shouldDirectUserToApplicationSettings) {
        openApplicationSettings(activity)
    }
}

/**
 * This function is used to check the location permissions are already granted or not.
 */
fun areLocationPermissionsAlreadyGranted(context: Context): Boolean {
    return ContextCompat.checkSelfPermission(
        context,
        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
}

private fun openApplicationSettings(activity: ComponentActivity) {
    Intent(
        Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
        Uri.fromParts("package", activity.packageName, null)
    ).also {
            startActivity(activity, it, null)
        }
}

private fun decideCurrentPermissionStatus(locationPermissionsGranted: Boolean,
                                          shouldShowPermissionRationale: Boolean): String {
    return if (locationPermissionsGranted) "Granted"
    else if (shouldShowPermissionRationale) "Rejected"
    else "Denied"
}