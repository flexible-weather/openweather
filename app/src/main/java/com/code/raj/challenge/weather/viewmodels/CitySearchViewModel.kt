package com.code.raj.challenge.weather.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.code.raj.challenge.weather.compose.model.CitySearchData
import com.code.raj.challenge.weather.domain.WeatherSearchUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CitySearchViewModel @Inject constructor(
    private val weatherSearchUseCase: WeatherSearchUseCase
) : ViewModel() {

    private val _searchCity = MutableStateFlow<List<CitySearchData>>(emptyList())
    val citySearchResult: StateFlow<List<CitySearchData>> = _searchCity

    fun searchCities(cityName: String) {
        viewModelScope.launch {
            weatherSearchUseCase.invoke(cityName)
                .collectLatest {
                    _searchCity.value = it
                }
        }
    }

    fun clearSearch() {
        _searchCity.value = emptyList()
    }
}