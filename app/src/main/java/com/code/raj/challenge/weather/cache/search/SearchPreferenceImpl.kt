package com.code.raj.challenge.weather.cache.search

import android.content.Context
import android.content.SharedPreferences
import android.content.SharedPreferences.Editor
import android.util.Log
import com.code.raj.challenge.weather.compose.model.CitySearchData
import com.google.gson.Gson
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

/**
 * Actual implementation of the local preference to handle recent searched city.
 */
class SearchPreferenceImpl @Inject constructor(
    @ApplicationContext context: Context
) : SearchPreference {
    private var sharedPreferences: SharedPreferences
    private var editor: Editor

    init {
        sharedPreferences = context.getSharedPreferences(CITY_SHARED_PREFS, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
    }

    override fun getRecentSearchCity(): CitySearchData? {
        val jsonString = sharedPreferences.getString(RECENT_SEARCH_CITY, "")
        jsonString?.let {
            Log.d("SearchPreferenceImpl", "GET JSON String----$jsonString")
            return Gson().fromJson(it, CitySearchData::class.java)
        }
        return null
    }

    override fun saveRecentSearchCity(searchData: CitySearchData) {
        val jsonString = Gson().toJson(searchData)
        Log.d("SearchPreferenceImpl", "SAVE JSON String----$jsonString")
        editor.putString(RECENT_SEARCH_CITY, jsonString)
        editor.commit()
    }

    companion object {
        const val CITY_SHARED_PREFS: String = "WeatherCitySearch"
        const val RECENT_SEARCH_CITY: String = "RecentSearchedCity"
    }
}