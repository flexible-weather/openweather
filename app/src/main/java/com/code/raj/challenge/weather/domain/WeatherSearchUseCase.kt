package com.code.raj.challenge.weather.domain

import com.code.raj.challenge.weather.compose.model.CitySearchData
import com.code.raj.challenge.weather.repository.WeatherSearchRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/**
 * This use case handles business logic for search screen.
 */
class WeatherSearchUseCase @Inject constructor(
    private val searchRepository: WeatherSearchRepository
) {

    suspend operator fun invoke(
        searchQuery: String,
    ): Flow<List<CitySearchData>> =
         searchRepository.getCitySearchResult(searchQuery)
}
