package com.code.raj.challenge.weather.compose.theme

import androidx.compose.ui.graphics.Color

val Black400 = Color(0xFF3A3A3A)
val Black700 = Color(0xFF0F0F14)
val White400 = Color(0xFFC5C5C5)