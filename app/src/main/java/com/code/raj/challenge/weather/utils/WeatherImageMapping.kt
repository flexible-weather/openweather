package com.code.raj.challenge.weather.utils

object WeatherImageMapping {
    fun getWeatherIconCode(code: Int): String {
        return when (code) {
            in 200..232 -> "11d"
            in 300..321 -> "09d"
            in 500..504 -> "10d"
            511, 520, 521, 522, 531 -> "09d"
            in 600..622 -> "13d"
            in 701..781 -> "50d"
            800 -> "01d"
            801 -> "02d"
            802 -> "03d"
            803, 804 -> "04d"
            else -> {"04n"}
        }
    }
}