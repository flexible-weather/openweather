package com.code.raj.challenge.weather.cache.search

import com.code.raj.challenge.weather.compose.model.CitySearchData

/**
 * Preferences to retrieve and save recent searched city data
 */
interface SearchPreference {
    fun getRecentSearchCity() : CitySearchData?
    fun saveRecentSearchCity(searchData: CitySearchData)
}