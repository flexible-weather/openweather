package com.code.raj.challenge.weather.compose.theme

import androidx.compose.material.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.sp
import com.code.raj.challenge.weather.R

val SourceSansProExtraLight = FontFamily(Font(R.font.sourcesanspro_extralight))
val SourceSansProRegular = FontFamily(Font(R.font.sourcesanspro_regular))
val SourceSansProSemiBold = FontFamily(Font(R.font.sourcesansoro_semibold))

val Typography = Typography(
    h1 = TextStyle(
        fontFamily = SourceSansProSemiBold,
        fontSize = 88.sp,
        letterSpacing = 0.5.sp
    ),
    subtitle1 = TextStyle(
        fontFamily = SourceSansProSemiBold,
        fontSize = 16.sp,
        letterSpacing = 0.sp
    ),
    body1 = TextStyle(
        fontFamily = SourceSansProExtraLight,
        fontSize = 14.sp,
        letterSpacing = 0.sp
    ),
    body2 = TextStyle(
        fontFamily = SourceSansProSemiBold,
        fontSize = 14.sp,
        letterSpacing = 0.sp
    ),
    caption = TextStyle(
        fontFamily = SourceSansProRegular,
        fontSize = 10.sp,
        letterSpacing = 0.sp
    )
)