package com.code.raj.challenge.weather.repository

import com.code.raj.challenge.weather.compose.model.CitySearchData
import kotlinx.coroutines.flow.Flow

/**
 * Data layer interface for the search screen.
 */
interface WeatherSearchRepository {
    suspend fun getCitySearchResult(cityName: String): Flow<List<CitySearchData>>
}