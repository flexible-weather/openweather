package com.code.raj.challenge.weather.cache.model.forecast

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Suppress("unused")
const val mockWeatherForecastResult = """
 {"cod":"200",
 "message":0,
 "cnt":5,
 "list":[{"dt":1706140800,
 "main":{"temp":250.9,"feels_like":246.16,"temp_min":250.65,"temp_max":250.9,
 "pressure":1040,"sea_level":1040,
 "grnd_level":650,"humidity":62,"temp_kf":0.25},
 "weather":[{"id":800,"main":"Clear",
 "description":"clear sky","icon":"01n"}],
 "clouds":{"all":0},"wind":{"speed":1.45,"deg":202,"gust":1.9},
 "visibility":10000,"pop":0,"sys":{"pod":"n"},
 "dt_txt":"2024-01-25 00:00:00"},
 {"dt":1706151600,
 "main":{"temp":251.53,"feels_like":245.58,
 "temp_min":251.53,"temp_max":251.78,"pressure":1039,
 "sea_level":1039,"grnd_level":651,"humidity":53,"temp_kf":-0.25},
 "weather":[{"id":800,"main":"Clear","description":"clear sky","icon":"01d"}],"clouds":{"all":0},"wind":{"speed":1.97,"deg":200,"gust":2.19},"visibility":10000,"pop":0,"sys":{"pod":"d"},"dt_txt":"2024-01-25 03:00:00"},{"dt":1706162400,"main":{"temp":257.3,"feels_like":250.3,"temp_min":257.3,"temp_max":257.3,"pressure":1031,"sea_level":1031,"grnd_level":652,"humidity":46,"temp_kf":0},"weather":[{"id":800,"main":"Clear","description":"clear sky","icon":"01d"}],"clouds":{"all":0},"wind":{"speed":3.75,"deg":211,"gust":4.37},"visibility":10000,"pop":0,"sys":{"pod":"d"},"dt_txt":"2024-01-25 06:00:00"},{"dt":1706173200,"main":{"temp":257.04,"feels_like":250.04,"temp_min":257.04,"temp_max":257.04,"pressure":1029,"sea_level":1029,"grnd_level":651,"humidity":52,"temp_kf":0},"weather":[{"id":800,"main":"Clear","description":"clear sky","icon":"01d"}],"clouds":{"all":0},"wind":{"speed":3.42,"deg":209,"gust":3.89},"visibility":10000,"pop":0,"sys":{"pod":"d"},"dt_txt":"2024-01-25 09:00:00"},{"dt":1706184000,"main":{"temp":251.98,"feels_like":245.18,"temp_min":251.98,"temp_max":251.98,"pressure":1035,"sea_level":1035,"grnd_level":648,"humidity":52,"temp_kf":0},"weather":[{"id":800,"main":"Clear","description":"clear sky","icon":"01n"}],"clouds":{"all":7},"wind":{"speed":2.43,"deg":203,"gust":2.18},"visibility":10000,"pop":0,"sys":{"pod":"n"},"dt_txt":"2024-01-25 12:00:00"}],"city":{"id":1529077,"name":"Xahbulak","coord":{"lat":42.4806,"lon":83.4755},"country":"CN","population":0,"timezone":28800,"sunrise":1706147234,"sunset":1706182133}}
"""

@Entity(tableName = "weather_forecast")
data class WeatherForecastResult(
    var cod: String? = null,
    var message: Int? = null,
    var cnt: Int? = 0,
    @SerializedName("list")
    var forecastList: ArrayList<ForecastList> = arrayListOf(),
    @Embedded(prefix = "forecast_")
    var city: City? = City(),
    var ttl: Long = System.currentTimeMillis(),
    var lat : String? = null,
    var lon : String? = null
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int? = 0
}