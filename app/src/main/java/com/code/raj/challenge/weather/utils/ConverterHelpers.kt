package com.code.raj.challenge.weather.utils

import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.util.Date
import java.util.Locale
import java.util.concurrent.TimeUnit

object DateTimeConverter {

    fun convertLongToDate(time: Long): String {
        val date = Date(TimeUnit.SECONDS.toMillis(time))
        val format = SimpleDateFormat("MMM dd, HH:mm aaa", Locale.US)
        return format.format(date)
    }

    fun convertLongToDay(date: String): String {
        val df = SimpleDateFormat("yyyy-mm-dd", Locale.US)
        val dateLong = df.parse(date)
        val format = SimpleDateFormat("MMM dd", Locale.US)
        return format.format(dateLong!!)
    }

    fun toTime(timestamp: Long, timeOffSet: Int): String {
        val local = LocalDateTime.ofEpochSecond(timestamp, 0,
            ZoneOffset.ofTotalSeconds(timeOffSet))
        return local.format(DateTimeFormatter.ofPattern("hh:mm"))
    }

    fun Double.roundTwo() = Math.round(this * 100.0) / 100.0

    fun Double.roundThree() = Math.round(this * 1000.0) / 1000.0
}
