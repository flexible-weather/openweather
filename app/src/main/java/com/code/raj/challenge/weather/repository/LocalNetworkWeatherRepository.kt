package com.code.raj.challenge.weather.repository

import com.code.raj.challenge.weather.cache.WeatherDao
import com.code.raj.challenge.weather.cache.model.WeatherResult
import com.code.raj.challenge.weather.cache.model.forecast.WeatherForecastResult
import com.code.raj.challenge.weather.utils.DateTimeConverter.roundThree
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Handles the logic to load weather data from the network if local cache is not available or expired.
 * Local cache mechanism is defaulted with 10 minutes.
 * Handles mechanism to delete the expired cache records if exist.
 */
class LocalNetworkWeatherRepository @Inject constructor(
    private val weatherNetworkService: WeatherNetworkService,
    private val weatherDao: WeatherDao
) : WeatherRepository {
    override val weatherData: Flow<WeatherResult?>
        get() = flow {
            delay(2000)
            emit(null)
        }

    override suspend fun getWeatherByLatLong(latitude: String, longitude: String) = flow {
        var weatherResult: WeatherResult? = weatherDao.getWeather(
            latitude.toDouble().roundThree(),
            longitude.toDouble().roundThree()
        ).first()
        val dataAge = weatherResult?.ttl ?: 0L
        if (!expired(dataAge)) {
            emit(weatherResult)
            return@flow
        } else {
            weatherDao.deleteWeather(latitude.toDouble(), longitude.toDouble())
        }
        weatherResult = weatherNetworkService.getWeatherByLatLng(latitude, longitude)
        weatherResult?.let {
            emit(it)
            weatherDao.insertWeather(it)
        }
    }.flowOn(Dispatchers.IO)

    override suspend fun forecastWeather(lat: String, long: String) = flow {
        var weatherForecastResult: WeatherForecastResult? =
            weatherDao.getWeatherForecast(lat, long).firstOrNull()
        val dataAge = weatherForecastResult?.ttl ?: 0L

        if (!expired(dataAge)) {
            emit(weatherForecastResult)
            return@flow
        } else {
            weatherDao.deleteWeatherForecast(lat, long)
        }
        weatherForecastResult = weatherNetworkService.forecastWeather(lat, long)

        emit(weatherForecastResult).also {
            weatherForecastResult.let { result ->
                weatherDao.insertWeatherForecast(result.copy(lat = lat, lon = long))
            }
        }
    }.flowOn(Dispatchers.IO)

    private fun expired(dataAge: Long): Boolean {
        return dataAge + TimeUnit.MINUTES.toMillis(10) < currentTimeMillis()
    }

    private var currentTimeMillis = { System.currentTimeMillis() }
}