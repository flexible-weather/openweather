package com.code.raj.challenge.weather.cache.model.forecast

import com.google.gson.annotations.SerializedName

data class Rain (

  @SerializedName("3h" ) var threeHour : Double? = null

)