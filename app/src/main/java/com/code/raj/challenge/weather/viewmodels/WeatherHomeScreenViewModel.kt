package com.code.raj.challenge.weather.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.code.raj.challenge.weather.cache.model.WeatherResult
import com.code.raj.challenge.weather.cache.model.forecast.WeatherForecastResult
import com.code.raj.challenge.weather.domain.SearchByCityUseCase
import com.code.raj.challenge.weather.domain.WeatherHomeUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WeatherHomeScreenViewModel @Inject constructor(
    private val weatherHomeUseCase: WeatherHomeUseCase,
    private val searchByCityUseCase: SearchByCityUseCase
) : ViewModel() {

    private val _weatherViewState = MutableStateFlow(ViewState())
    val viewState:StateFlow<ViewState> = _weatherViewState

    init {
        loadData()
    }

    fun refresh() {
        _weatherViewState.value = _weatherViewState.value.copy(isLoading = true)
        loadData()
    }

    fun loadData() {
        viewModelScope.launch {
            weatherHomeUseCase.invoke().collectLatest {
                _weatherViewState.value = it
            }
        }
    }

    fun searchWeatherByCity(city: String, latitude: String, longitude: String) {
        viewModelScope.launch {
            searchByCityUseCase.invoke(city, latitude, longitude).collectLatest {
                _weatherViewState.value = it
            }
        }
    }

    data class ViewState(
        val isLoading: Boolean = true,
        val weatherResult: WeatherResult? = null,
        val forecastResponse: WeatherForecastResult? = null
    )
}