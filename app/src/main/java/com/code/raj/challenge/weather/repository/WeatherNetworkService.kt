package com.code.raj.challenge.weather.repository

import com.code.raj.challenge.weather.cache.model.CitySearchResponse
import com.code.raj.challenge.weather.cache.model.WeatherResult
import com.code.raj.challenge.weather.cache.model.forecast.WeatherForecastResult
import com.code.raj.challenge.weather.cache.model.geo.GeoLocationResponse
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Retrofit API declaration for Open Weather API
 */
interface WeatherNetworkService {

    @GET("/data/2.5/weather?units=imperial")
    suspend fun getWeatherByLatLng(
        @Query("lat") lat: String?,
        @Query("lon") lon: String?
    ): WeatherResult?

    @GET("data/2.5/find?")
    suspend fun searchCities(
        @Query("q") cityName: String?
    ): CitySearchResponse

    @GET("data/2.5/forecast?units=imperial")
    suspend fun forecastWeather(
        @Query("lat") lat: String?,
        @Query("lon") lon: String?
    ): WeatherForecastResult

    @GET("/geo/1.0/reverse")
    suspend fun reverseGeoLocation(
        @Query("lat") lat: String?,
        @Query("lon") lon: String?
    ): List<GeoLocationResponse>
}