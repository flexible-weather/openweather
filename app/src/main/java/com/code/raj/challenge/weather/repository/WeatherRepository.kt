package com.code.raj.challenge.weather.repository

import com.code.raj.challenge.weather.cache.model.WeatherResult
import com.code.raj.challenge.weather.cache.model.forecast.WeatherForecastResult
import kotlinx.coroutines.flow.Flow

/**
 * Data layer interface for the home screen.
 */
interface WeatherRepository {
    val weatherData: Flow<WeatherResult?>
    suspend fun getWeatherByLatLong(latitude: String, longitude: String): Flow<WeatherResult?>
    suspend fun forecastWeather(lat: String, long: String): Flow<WeatherForecastResult?>
}