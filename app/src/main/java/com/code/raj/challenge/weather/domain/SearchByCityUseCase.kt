package com.code.raj.challenge.weather.domain

import com.code.raj.challenge.weather.cache.model.Coord
import com.code.raj.challenge.weather.cache.search.SearchPreference
import com.code.raj.challenge.weather.compose.model.CitySearchData
import com.code.raj.challenge.weather.viewmodels.WeatherHomeScreenViewModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class SearchByCityUseCase @Inject constructor(
    private val searchPreference: SearchPreference,
    private val weatherHomeUseCase: WeatherHomeUseCase
) {
    suspend operator fun invoke(city: String, latitude: String, longitude: String): Flow<WeatherHomeScreenViewModel.ViewState> {
        searchPreference.saveRecentSearchCity(
            CitySearchData(address = city,
                coord = Coord(lat = latitude.toDouble(), lon = longitude.toDouble())
            )
        )
        return weatherHomeUseCase.loadWeatherByPreferredLocation()
    }
}