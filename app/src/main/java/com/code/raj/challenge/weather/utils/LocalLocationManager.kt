package com.code.raj.challenge.weather.utils

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Looper
import android.util.Log
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.Priority
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class LocalLocationManager @Inject constructor(
    @ApplicationContext val context: Context
) {

    fun fetchUpdates(): Flow<Location> {
        return callbackFlow {
            val locationClient = LocationServices.getFusedLocationProviderClient(context)
            val locationRequest = LocationRequest.Builder(
                Priority.PRIORITY_HIGH_ACCURACY,
                TimeUnit.MINUTES.toMillis(10)
            ).build()
            if (ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED) {
                val callBack = object : LocationCallback() {
                    override fun onLocationResult(locationResult: LocationResult) {
                        super.onLocationResult(locationResult)
                        Log.d("LocationClient", "LastLocation : $locationResult")
    //                    val location = locationResult.lastLocation
                        locationResult.lastLocation?.let { trySend(it) }
                    }
                }

                locationClient.requestLocationUpdates(
                    locationRequest,
                    callBack,
                    Looper.getMainLooper()
                )
                awaitClose { locationClient.removeLocationUpdates(callBack) }
    //            val location: Location? =
    //                locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
    //            trySend(location)
            }
        } .distinctUntilChanged()
    }

    fun areLocationPermissionGranted() = ActivityCompat.checkSelfPermission(
        context,
        Manifest.permission.ACCESS_FINE_LOCATION
    ) == PackageManager.PERMISSION_GRANTED
}