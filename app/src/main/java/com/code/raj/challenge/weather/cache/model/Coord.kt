package com.code.raj.challenge.weather.cache.model

data class Coord (
  var lat : Double? = null,
  var lon : Double? = null
)