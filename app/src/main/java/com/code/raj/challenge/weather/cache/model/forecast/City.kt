package com.code.raj.challenge.weather.cache.model.forecast

import androidx.room.Embedded
import com.code.raj.challenge.weather.cache.model.Coord

data class City(
  var id: Int? = null,
  var name: String? = null,
  @Embedded("coord_")
  var coord: Coord? = Coord(),
  var country: String? = null,
  var population: Int? = null,
  var timezone: Int? = null,
  var sunrise: Int? = null,
  var sunset: Int? = null
)