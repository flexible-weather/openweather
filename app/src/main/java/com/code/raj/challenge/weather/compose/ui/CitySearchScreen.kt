package com.code.raj.challenge.weather.compose.ui

import android.annotation.SuppressLint
import android.util.Log
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.SearchBar
import androidx.compose.material3.SearchBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.code.raj.challenge.weather.R
import com.code.raj.challenge.weather.compose.model.CitySearchData
import com.code.raj.challenge.weather.viewmodels.CitySearchViewModel

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun SearchCityScreen(
    viewModel: CitySearchViewModel,
    popBackWithResult: (CitySearchData) -> Unit,
    popUp: () -> Unit
) {
    Scaffold(
        modifier = Modifier
            .fillMaxSize(),
        topBar = { SearchToolbar(popUp) },
        backgroundColor = Color.Transparent
    ) {
        CitySearchBar(viewModel = viewModel, popBackWithResult = popBackWithResult)
    }
}

@Composable
private fun SearchToolbar(popUp: () -> Unit) {
    TopAppBar(
        modifier = Modifier.statusBarsPadding(),
        title = {
            Text(
                text = stringResource(id = R.string.search_title),
                style = MaterialTheme.typography.h6,
                color = MaterialTheme.colors.onPrimary
            )
        },
        navigationIcon = {
            IconButton(onClick = popUp) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_arrow_back_24),
                    contentDescription = null,
                    tint = MaterialTheme.colors.onPrimary
                )
            }
        },
        actions = {},
        backgroundColor = Color.Transparent,
        elevation = 0.dp
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun CitySearchBar(viewModel: CitySearchViewModel, popBackWithResult: (CitySearchData) -> Unit) {
    var text by remember { mutableStateOf("") } // Query for SearchBar
    var active by remember { mutableStateOf(false) } // Active state for SearchBar

    Column(modifier = Modifier.padding(horizontal = 10.dp)) {
        SearchBar(modifier = Modifier.fillMaxWidth(),
            colors = SearchBarDefaults.colors(
                containerColor = MaterialTheme.colors.primaryVariant,
                dividerColor = MaterialTheme.colors.onPrimary,
                inputFieldColors = SearchBarDefaults.inputFieldColors(
                    unfocusedTextColor = MaterialTheme.colors.onPrimary,
                    cursorColor = MaterialTheme.colors.onPrimary,
                    focusedTextColor = MaterialTheme.colors.onPrimary
                ),
            ),
            query = text,
            onQueryChange = {
                text = it
            },
            onSearch = {
                Log.d("CitySearchScreen", "OnSearch Triggered---------- $it")
                viewModel.searchCities(it)
                active = true
            },
            active = active,
            onActiveChange = {
                active = false
            },
            placeholder = {
                Text(text = "Enter City", color = MaterialTheme.colors.onPrimary)
            },
            leadingIcon = {
                Icon(
                    imageVector = Icons.Default.Search,
                    contentDescription = "Search icon",
                    tint = MaterialTheme.colors.onPrimary
                )
            },
            trailingIcon = {
                if (text.isNotEmpty()) {
                    Icon(
                        modifier = Modifier.clickable {
                            if (text.isNotEmpty()) {
                                text = ""
                                viewModel.clearSearch()
                                active = false
                            } else {
                                active = false
                            }
                        },
                        imageVector = Icons.Default.Close,
                        contentDescription = "Close icon",
                        tint = MaterialTheme.colors.onPrimary
                    )
                }
            }
        ) {
            SearchedList(viewModel, popBackWithResult)
        }
    }
}

@Composable
fun SearchedList(viewModel: CitySearchViewModel, popBackWithResult: (CitySearchData) -> Unit) {
    val searchResultsState = viewModel.citySearchResult.collectAsStateWithLifecycle()
    LazyColumn(
        modifier = Modifier.fillMaxSize(),
        contentPadding = PaddingValues(vertical = 16.dp)
    ) {
        items(searchResultsState.value) {
            with(it) {
                Row(modifier = Modifier
                    .padding(all = 5.dp)
                    .clickable(
                        onClick = {
                            popBackWithResult(it)
                        }
                    )
                ) {
                    Text(
                        modifier = Modifier
                            .padding(start = 16.dp, top = 10.dp, bottom = 10.dp)
                            .fillMaxWidth(),
                        text = address,
                        style = MaterialTheme.typography.subtitle1,
                        color = MaterialTheme.colors.onPrimary
                    )
                }
                Divider(
                    color = MaterialTheme.colors.onPrimary
                )
            }
        }
    }
}