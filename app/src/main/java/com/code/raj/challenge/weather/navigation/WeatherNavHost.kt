package com.code.raj.challenge.weather.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost

@Composable
fun WeatherNavHost(
    navController: NavHostController,
    startDestination: String = WEATHER_HOME_ROUTE,
) {
    NavHost(navController = navController,
        startDestination = startDestination) {
        homeScreen(navController)
        citySearchScreen(navController)
    }
}