package com.code.raj.challenge.weather.cache.model


@Suppress("unused")
private const val SearchMockResponse: String = """
{
    "message": "accurate",
    "cod": "200",
    "count": 4,
    "list": [
        {
            "id": 4350049,
            "name": "California",
            "coord": {
                "lat": 38.3004,
                "lon": -76.5074
            },
            "main": {
                "temp": 293.61,
                "feels_like": 294.22,
                "temp_min": 292.64,
                "temp_max": 294.18,
                "pressure": 1012,
                "humidity": 96
            },
            "dt": 1690801246,
            "wind": {
                "speed": 1.54,
                "deg": 150
            },
            "sys": {
                "country": "US"
            },
            "rain": null,
            "snow": null,
            "clouds": {
                "all": 75
            },
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04d"
                }
            ]
        },
        {
            "id": 5182733,
            "name": "California",
            "coord": {
                "lat": 40.0656,
                "lon": -79.8917
            },
            "main": {
                "temp": 289.58,
                "feels_like": 289.84,
                "temp_min": 287.92,
                "temp_max": 290.93,
                "pressure": 1017,
                "humidity": 98
            },
            "dt": 1690803505,
            "wind": {
                "speed": 1.43,
                "deg": 1
            },
            "sys": {
                "country": "US"
            },
            "rain": null,
            "snow": null,
            "clouds": {
                "all": 37
            },
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03d"
                }
            ]
        },
        {
            "id": 4379545,
            "name": "California",
            "coord": {
                "lat": 38.6275,
                "lon": -92.5666
            },
            "main": {
                "temp": 293.48,
                "feels_like": 293.87,
                "temp_min": 292.88,
                "temp_max": 294.21,
                "pressure": 1019,
                "humidity": 88
            },
            "dt": 1690801250,
            "wind": {
                "speed": 0.45,
                "deg": 312
            },
            "sys": {
                "country": "US"
            },
            "rain": null,
            "snow": null,
            "clouds": {
                "all": 17
            },
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02n"
                }
            ]
        },
        {
            "id": 5332921,
            "name": "California",
            "coord": {
                "lat": 37.2502,
                "lon": -119.7513
            },
            "main": {
                "temp": 296.96,
                "feels_like": 296.18,
                "temp_min": 289.59,
                "temp_max": 300.08,
                "pressure": 1022,
                "humidity": 30
            },
            "dt": 1690804065,
            "wind": {
                "speed": 1.77,
                "deg": 51
            },
            "sys": {
                "country": "US"
            },
            "rain": null,
            "snow": null,
            "clouds": {
                "all": 77
            },
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04n"
                }
            ]
        }
    ]
}
"""

data class CitySearchResponse(
    var message: String = "",
    var cod: String = "",
    var count: Int = 0,
    var list: ArrayList<WeatherResult> = arrayListOf(),
)


